#include "base_solver.h"

namespace srrg_nicp {

using namespace srrg_core;

BaseSolver::BaseSolver() {
  _H.setZero();
  _b.setZero();
  _T.setIdentity();
  _error = 0;
  _reference = 0;
  _current = 0;

  _omega_tainted = true;
  _has_prior = false;

  _max_error = .01;
  _damping = 100;

  _information_matrix.setZero();
}

BaseSolver::~BaseSolver() {}

void BaseSolver::setStaticModel(const Cloud3D *m) { this->setReferenceModel(m); }

void BaseSolver::setReferenceModel(const Cloud3D *m) {
  _reference = m;
  _omega_tainted = true;
  _has_prior = false;
  _information_matrix.setZero();
}

void BaseSolver::setMovingModel(const Cloud3D *m) { this->setCurrentModel(m); }

void BaseSolver::setCurrentModel(const Cloud3D *m) {
  _current = m;
  _has_prior = false;
  if (m) {
    _errors.resize(m->size());
    std::fill(_errors.begin(), _errors.end(), -1.0f);
  }
  _information_matrix.setZero();
}


}
