#include "base_projector.h"
#include <stdexcept>
#include "srrg_image_utils/point_image_utils.h"
#include "srrg_image_utils/depth_utils.h"

#include <omp.h>

namespace srrg_nicp {

  using namespace std;
  using namespace srrg_core;
  

  BaseProjector::BaseProjector(){
    _image_cols=640;
    _image_rows=480;
    _min_distance = 0.4;
    _max_distance = 3;
    _offset.setIdentity();
    _inverse_offset.setIdentity();
    _camera_info = 0;
    setIncidenceAngle(0.4 * M_PI);

    _nb_window = 3;
    _cp_window = 3;
    _cp_max_distance = 0.3;
    _raw_depth_scale = 1e-3;
    _information_criterion = Constant;
  }

  void BaseProjector::setImageSize(int r, int c) {
    _image_rows = r;
    _image_cols = c;
  }

  void BaseProjector::setCameraInfo(BaseCameraInfo* camera_info){
    _camera_info = camera_info;
    _raw_depth_scale = camera_info->depthScale();
    setOffset(_camera_info->offset());
  }
  
  void BaseProjector::unproject(Cloud3D& cloud,
				const RawDepthImage& depth_image,
				const RGBImage& rgb_image) {
    // generate the points applying the inverse depth model
    rgb_image.convertTo(_rgb, CV_8UC3);
    unprojectPoints(depth_image);
    computeNormals(cloud, depth_image);
    cloud.transformInPlace(_offset);
  }

    void BaseProjector::computeNormals(Cloud3D& cloud, const RawDepthImage& depth_image){
    computeSimpleNormals(_normals, _points, _cp_window, _cp_window, _cp_max_distance);
    normalBlur(_smoothed_normals, _normals, _nb_window);
    
    int rstart = _nb_window+_cp_window;
    int rend = depth_image.rows - _nb_window+_cp_window;

    int cstart = _nb_window+_cp_window;
    int cend = depth_image.cols - _nb_window+_cp_window;

    cloud.resize( (rend-rstart)*(cend-cstart) );
    int k = 0;
    //cerr << "nz:" << nz <<endl;
    for (int r=rstart; r<rend; r++){
      for (int c=cstart; c<cend; c++){
	unsigned short d = depth_image.at<unsigned short>(r,c);
	if (!d)
	  continue;
	const cv::Vec3f& _p = _points.at<cv::Vec3f>(r,c);
	float p_norm=norm(_p);
	if (p_norm==0)
	  continue;
	const cv::Vec3f& _n = _smoothed_normals.at<cv::Vec3f>(r,c);
	if (norm(_n)<1e-3)
	  continue;
	float information = 0;
	switch(_information_criterion){
	case Constant: 
	  information = 1;
	  break;
	case InverseDepth: 
	  information = 1./_p[2];
	  break;
	case InverseDistance:
	  information = 1./p_norm;
	}
	
	Eigen::Vector3f rgbv(0,0,0);
	if (_rgb.rows && _rgb.cols){
	  const cv::Vec3f &rgb = _rgb.at<cv::Vec3f>(r,c);
	  rgbv = Eigen::Vector3f(rgb[0]/255.0, rgb[1]/255.0, rgb[2]/255.0);
	}
	cloud[k] = RichPoint3D(Eigen::Vector3f(_p[0], _p[1], _p[2]), Eigen::Vector3f(_n[0], _n[1], _n[2]), information, rgbv);
	k++;
      }
    }
    cloud.resize(k);
  }

}
