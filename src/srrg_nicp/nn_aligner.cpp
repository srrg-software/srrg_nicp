#include "nn_aligner.h"
#include <stdexcept>

namespace srrg_nicp {

using namespace std;
using namespace Eigen;


  NNAligner::NNAligner(BaseProjector* p) :
    BaseAligner(new Solver, p),
    _finder(_solver)
  {
    _solver->setMaxError(.01);
    _solver->setDamping(100);
    _solver->setGICP(false);
    setMaxDistance(0.1);
    _iterations = 10;
    _reference_compression_enabled=true;
    _voxelize_resolution=0.01;
  }

  NNAligner::~NNAligner() {}

  float NNAligner::maxDistance() const {return _finder.pointsDistance(); }

  void NNAligner::setMaxDistance(float md)  {_finder.setPointsDistance(md); }

  void NNAligner::align(const Eigen::Isometry3f& initial_guess,
 			const srrg_core::Matrix6f& initial_guess_information){
    using namespace std;
    if (! _solver->referenceModel()) {
      throw std::runtime_error("NNAligner: align(), reference model not set");
    }
    if (!_solver->currentModel()){
      throw std::runtime_error("NNAligner: align(), current model not set");
    }
    if (initial_guess_information == srrg_core::Matrix6f::Zero()) {
      _solver->setT(initial_guess);
    } else 
      _solver->setT(initial_guess,initial_guess_information);

    const srrg_core::Cloud3D* saved_reference = _solver->referenceModel();
    const srrg_core::Cloud3D* saved_current = _solver->currentModel();
    
    if (_reference_compression_enabled && _projector) {

      // culling non visible points in a wider frustum
      int rows=_projector->imageRows();
      int cols=_projector->imageCols();
      int hrows = rows*_reference_canvas_scale;
      int hcols = cols*_reference_canvas_scale;
      _projector->pushState();
      _projector->setImageSize(hrows,hcols);
      _projector->scaleCamera(1);
      _projector->project(_reference_buffer_hres,
                          _reference_indices_hres,
                          Eigen::Isometry3f::Identity(),
                          *_solver->referenceModel());
      _projector->popState();

      // copy current int local variable
      _compressed_current.reserve(hrows*hcols);
      _compressed_current.resize(_solver->currentModel()->size());
      std::copy(_solver->currentModel()->begin(),
                _solver->currentModel()->end(),
                _compressed_current.begin());
      
      // copy reference in current var
      _compressed_reference.reserve(hrows*hcols);
      _compressed_reference.resize(hrows*hcols);
      size_t k = 0;
      for (int r = 0; r<_reference_indices_hres.rows; ++r){
        int* ref_ptr=_reference_indices_hres.ptr<int>(r);
        for (int c = 0; c<_reference_indices_hres.cols; ++c, ++ref_ptr){
          int& idx = *ref_ptr;
          if (idx<0)
            continue;
          _compressed_reference[k]=saved_reference->at(idx);
          idx=k;
          ++k;
        }
      }

      _compressed_reference.resize(k);

      // voxelizing
      if (_voxelize_resolution>0.f) {
        _compressed_reference.voxelize(_voxelize_resolution);
        _compressed_current.voxelize(_voxelize_resolution);
      }
      _solver->setReferenceModel(&_compressed_reference);
      _solver->setCurrentModel(&_compressed_current);
      
    }
    _finder.init(_reference_unchanged_hint);
    callTriggers(BaseAligner::Initialized);
    for(int i = 0; i<_iterations; i++){
      callTriggers(BaseAligner::Iteration);
      _finder.compute();
      callTriggers(BaseAligner::Correspondences);
      const BaseCorrespondenceFinder::CorrespondenceVector& corr=_finder.correspondences();
      bool computeStats = (i == _iterations-1);
      _solver->oneRound(corr, computeStats);
      callTriggers(BaseAligner::Optimization);
    }
    _solver->setReferenceModel(saved_reference);
    _solver->setCurrentModel(saved_current);
  }
}
