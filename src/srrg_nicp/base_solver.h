#pragma once

#include <srrg_types/defs.h>

#include "base_correspondence_finder.h"
#include "pinhole_projector.h"

namespace srrg_nicp {

class BaseSolver {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  BaseSolver();
  virtual ~BaseSolver();

  virtual void setStaticModel(const srrg_core::Cloud3D *m);
  virtual void setReferenceModel(const srrg_core::Cloud3D *m);
  virtual const srrg_core::Cloud3D *staticModel() const { return _reference; }
  virtual const srrg_core::Cloud3D *referenceModel() const {
    return _reference;
  }

  virtual void setMovingModel(const srrg_core::Cloud3D *m);
  virtual void setCurrentModel(const srrg_core::Cloud3D *m);
  virtual const srrg_core::Cloud3D *movingModel() const { return _current; }
  virtual const srrg_core::Cloud3D *currentModel() const { return _current; }

  //! system matrix
  inline const srrg_core::Matrix6f &H() const { return _H; }

  //! coefficient vector
  inline const srrg_core::Vector6f &b() const { return _b; }

  //! current transform
  inline const Eigen::Isometry3f &T() const { return _T; }

  //! sets the current transform
  inline void setT(const Eigen::Isometry3f &T_) { _T = T_; }

  virtual void setT(const Eigen::Isometry3f &T_,
                    const srrg_core::Matrix6f &info) {}

  //! returns the chi2 after the current linearization
  inline float error() const { return _error; }

  //! returns the information matrix of the solution, computed from the hessian
  //! must be called after linearize();
  const srrg_core::Matrix6f &informationMatrix() const {
    return _information_matrix;
  }

  //! chi2 of every point
  inline const std::vector<float> &errors() const { return _errors; }

  //! damping in the solution of the linear system
  inline float damping() const { return _damping; }

  //! set the damping
  inline void setDamping(float d) { _damping = d; }

  //! max error after which the robust kernel will kick anc regard the point as
  //! outlier
  inline float maxError() const { return _max_error; }
  //! set the max error
  inline void setMaxError(float me) { _max_error = me; }

  //! sets gicp mode (ignores the normals in the error vector)
  void setGICP(bool gicp) { _use_only_points = gicp; }

  virtual void oneRound(bool computeStats = false) {}
  virtual void oneRound(
      const BaseCorrespondenceFinder::CorrespondenceVector &correspondences,
      bool computeStats = false) {}

  virtual void setReferencePointsHint(
      const std::vector<int> &currentPointsHint) {}

  virtual srrg_core::Matrix6f  remapInformationMatrix(const Eigen::Isometry3f& T) {}
  
 protected:
  bool _use_only_points;
  srrg_core::Matrix6f _information_matrix;
  srrg_core::Matrix6f _H;
  srrg_core::Vector6f _b;
  float _max_error;
  std::vector<float> _errors;
  float _error;
  float _damping;
  Eigen::Isometry3f _T;  // position of the world w.r.t the camera
  bool _omega_tainted;

  const srrg_core::Cloud3D *_reference, *_current;
  bool _has_prior;
};
}
