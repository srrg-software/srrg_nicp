#include "nn_correspondence_finder.h"
#include <srrg_system_utils/system_utils.h>

#ifdef _NN_GO_PARALLEL_
#include <omp.h>
#endif

namespace srrg_nicp {

  using namespace srrg_core;
  
  NNCorrespondenceFinder::NNCorrespondenceFinder(BaseSolver* s):
    BaseCorrespondenceFinder(s) {
    _kdtree = 0;
    _normal_scaling = 1;
    _leaf_range = 0.2;
    _max_distance = 1e9;
  }

  NNCorrespondenceFinder::~NNCorrespondenceFinder() {
    if (_kdtree) {      
      delete _kdtree;
    }
    _kdtree = 0;
  }

  void model2linear(KDTree<float, NNCF_KDTREE_DIM>::VectorTDVector& dest,
		    const Cloud3D& src, float nscale){
    int k = 0;
    dest.resize(src.size()); // relies on the packetization of the data
#ifdef _NN_GO_PARALLEL_
    #pragma omp parallel 
#endif
    for (size_t i = 0; i < src.size(); ++i) {
      dest[i].head<3>() = src[i].point();
      dest[i].tail<3>() = src[i].normal() * nscale;      
    }
  }

  void NNCorrespondenceFinder::init(bool reference_unchanged_hint){
    if (! _solver->referenceModel()){
      throw std::runtime_error("NNCorrespondenceFinder::init(), no reference model in solver");
    }

    if (! reference_unchanged_hint) {
      const Cloud3D& ref = *_solver->referenceModel();

      model2linear(_reference_points, ref, _normal_scaling);

      //std::cerr << "destroyng tree " << std::endl;
      if (_kdtree)
	delete(_kdtree);
      _kdtree = 0;
      //std::cerr << "done " << std::endl;
    
      _kdtree = new KDTree<float, NNCF_KDTREE_DIM>(_reference_points, _leaf_range);
    }

    if (! _solver->currentModel()){
      throw std::runtime_error("NNCorrespondenceFinder::init(), no reference model in solver");
    }

    // allocate the workspace for the current model and for the search
    const Cloud3D& curr = *_solver->currentModel();
    _correspondences.reserve(curr.size());
  }

  void NNCorrespondenceFinder::compute(){
    using namespace std;
    const Cloud3D& curr = *_solver->currentModel();
    const Cloud3D& ref = *_solver->referenceModel();
    Eigen::Isometry3f T = _solver->T();
    Eigen::Matrix3f sR = T.linear() * _normal_scaling;
    float ndist = cos(_normal_angle);
    float pdist = _points_distance * _points_distance;

    // scan through the returned indices and compute the correspondences
    _correspondences.resize(curr.size());

    int count = 0;

    #ifdef _NN_GO_PARALLEL_
    int num_threads=omp_get_max_threads();
    int starts[num_threads];
    int ends[num_threads];
    starts[0]=0;
    ends[0]=0;
    int num_points_per_thread=curr.size()/num_threads;
    for (int thread_num=1; thread_num<num_threads; ++thread_num){
      starts[thread_num] = ends[thread_num] = starts[thread_num-1]+num_points_per_thread;
    }
    #pragma omp parallel for
    #endif
    for(size_t i = 0; i < curr.size(); ++i) {
      // remap the current point, given the transform
      const RichPoint3D& p=curr[i];
      const Eigen::Vector3f cn = T.linear() * curr[i].normal();
      const Eigen::Vector3f cp = T * curr[i].point();

      // construct the 6D query point scaling the normal
      KDTree<float, NNCF_KDTREE_DIM>::VectorTD answer;
      KDTree<float, NNCF_KDTREE_DIM>::VectorTD query;
      query.head<3>()=cp;
      query.tail<3>()=_normal_scaling*cn;

      // perform the query
      int index;
      float approx_distance;
      approx_distance = _kdtree->findNeighbor(answer, index, query, _max_distance);

      // if no point skip
      if (index < 0) {
	continue;
      }
      
      const Eigen::Vector3f& rn = ref[index].normal();
      const Eigen::Vector3f& rp = ref[index].point();
      if ((cp - rp).squaredNorm() > pdist) {
	continue;
      }

      if (srrg_core::isNan(rn) || srrg_core::isNan(cn)) { 
	throw std::runtime_error("nan detected");
	continue;
      }
      if (cn.dot(rn) < ndist) {
	continue;
      }
      #ifdef _NN_GO_PARALLEL_
      int num_thread=omp_get_thread_num();
      int& dest_count=ends[num_thread];
      _correspondences[dest_count++]=std::make_pair(index, i);
      #else
      _correspondences[count++]=std::make_pair(index, i);
      #endif
    }
    #ifdef _NN_GO_PARALLEL_
    for (int thread_num=1; thread_num<num_threads; ++thread_num){
      int start=starts[thread_num];
      int end=ends[thread_num];
      for (int point_num=start; point_num<end; ++point_num, ++count)
	_correspondences[count]=_correspondences[point_num];
    }
    #endif
    _correspondences.resize(count);
  }

}
