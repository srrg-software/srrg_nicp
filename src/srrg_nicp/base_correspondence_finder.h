#pragma once

#include <utility>

#include <srrg_types/defs.h>

namespace srrg_nicp {
  class BaseSolver;

  //! abstract correspondence finder
  class BaseCorrespondenceFinder {
  public:
    //! this is the single correspondence
    //! first-> reference point index
    //! second-> current point index
    typedef std::pair <int,int> Correspondence;
    typedef std::vector<Correspondence> CorrespondenceVector;
    

    BaseCorrespondenceFinder(BaseSolver* s);

    //~ return the solver pbject linked to this
    inline BaseSolver& solver() {return *_solver;}

    //! angle between normals to consider as correspondences
    inline float normalAngle() const {return _normal_angle;}
    inline void setNormalAngle(float a)  {_normal_angle = a;}

    //! distance between corresponding points
    inline float pointsDistance() const {return _points_distance;}
    inline void setPointsDistance(float d)  {_points_distance = d;}

    //! call this once before staring an alignment
    virtual void init(bool reference_unchanged_hint=false) = 0;

    //! does the computation (to be overridden in base classes. It puts the resuld in  _correspondences)
    virtual void compute() = 0;

    virtual ~BaseCorrespondenceFinder();
    //! returns the corresoidences
    inline const CorrespondenceVector& correspondences() const { return _correspondences; }
  protected:
    CorrespondenceVector _correspondences;
    BaseSolver* _solver;
    float _normal_angle, _points_distance;

  };
}
