#pragma once

#include "base_correspondence_finder.h"
#include "unscented.h"

#include "base_solver.h"

namespace srrg_nicp {

/**
    Implements an iterative least squares solver for 3D points with
    normal. <br> The use of this class is
    straightforward.  After construction the object has to be loaded
    with two models: the reference and the current.  Then repeated
    calls to oneIteration() refine an inner transformation. After
    each iteration, the user can monitor the state of convergence by
    checking the cumulative error, the number and the identity of
    inliers.

  */
class Solver : public BaseSolver {
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  typedef _SigmaPoint<srrg_core::Vector6f> SigmaPoint;
  typedef std::vector<SigmaPoint, Eigen::aligned_allocator<SigmaPoint> >
      SigmaPointVector;

  Solver();
  virtual ~Solver();

  //! computes the system matrix and the coefficient vector of a least squares
  //! problem
  //! it does side effect in the _H and _b variables of this class
  virtual void linearize(
      const BaseCorrespondenceFinder::CorrespondenceVector &correspondences);

  //! omega of the planar patches (gets rotated to be aligned to the normal)
  inline const Eigen::Matrix3f &flatOmega() const { return _flat_omega; }
  inline void setFlatOmega(const Eigen::Matrix3f &o) { _flat_omega = o; }

  //! omega of the normals to the planar patches (gets rotated to be aligned on
  //! the normal)
  inline const Eigen::Matrix3f &longLinearOmega() const {
    return _long_linear_omega;
  }
  inline void setLongLinearOmega(const Eigen::Matrix3f &o) {
    _long_linear_omega = o;
  }

  //! sets the current transform, and the prior
  inline void setT(const Eigen::Isometry3f &T_,
                   const srrg_core::Matrix6f &info) {
    _T = T_;
    _prior_transform = T_;
    _inverse_prior_transform = T_.inverse();
    _prior_information = info;
    _has_prior = true;
  }

  // call to oneRound without correspondences, nothing to do
  virtual void oneRound();

  // one round of linearization/solution. Side effect in T()
  virtual void oneRound(
      const BaseCorrespondenceFinder::CorrespondenceVector &correspondences,
      bool computeStats = false);

  //! gives a hint to the system on which omegas need to be recomputed
  //! needs to be called after setting the current model
  void setReferencePointsHint(const std::vector<int> &currentPointsHint);

  inline const Eigen::Isometry3f &priorTransform() const {
    return _prior_transform;
  }
  inline const srrg_core::Matrix6f &priorInformationMatrix() const {
    return _prior_information;
  }

  inline void clearPrior() { _has_prior = false; }

  srrg_core::Matrix6f  remapInformationMatrix(const Eigen::Isometry3f& T);

  
protected:
  srrg_core::Matrix3fVector _omega_points, _omega_normals;
  Eigen::Matrix3f _flat_omega;
  Eigen::Matrix3f _long_linear_omega;
  Eigen::Isometry3f _prior_transform;
  srrg_core::Matrix6f _prior_information;

  void errorAndJacobian(Eigen::Vector3f &pointError,
                        Eigen::Vector3f &normalError, srrg_core::Matrix6f &J,
                        const Eigen::Vector3f &referencePoint,
                        const Eigen::Vector3f &referenceNormal,
                        const Eigen::Vector3f &currentPoint,
                        const Eigen::Vector3f &currentNormal) const;

  void errorAndJacobianPoint(Eigen::Vector3f &pointError,
                             srrg_core::Matrix3_6f &J,
                             const Eigen::Vector3f &referencePoint,
                             const Eigen::Vector3f &currentPoint) const;

  // computes the hessian and the coefficient vector of the reference points
  // between imin and imax
  // it also computes the error and the number of inliers
  virtual void linearize(
      srrg_core::Matrix6f &H, srrg_core::Vector6f &b, float &error,
      const BaseCorrespondenceFinder::CorrespondenceVector &correspondences,
      size_t imin = 0, size_t imax = std::numeric_limits<size_t>::max());

  // if no argument is given it updates all omegas, otherwise it updates the
  // omegas of the passed vector of indice
  void computeOmegas(const std::vector<int> updateList = std::vector<int>());

  // computes the informatio matrix
  void computeInformationMatrix();

private:
  srrg_core::Vector6f
  priorError(const srrg_core::Vector6f &dt = srrg_core::Vector6f::Zero());
  srrg_core::Matrix6f priorJacobian();
  SigmaPointVector _raw_sigma_points;
  Eigen::Isometry3f _inverse_prior_transform;
};
}
