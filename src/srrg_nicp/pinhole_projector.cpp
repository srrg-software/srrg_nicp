#include "pinhole_projector.h"
#include <srrg_types/pinhole_camera_info.h>
#include <stdexcept>
#include <cmath>
#include <iostream>

#ifdef _GO_PARALLEL_
#include <omp.h>
#endif

namespace srrg_nicp {

  using namespace std;
  using namespace srrg_core;
  
  void PinholeProjector::allocateBuffers() const {
    size_t current_size = _image_cols * _image_rows;
    if (current_size>_max_image_size){
      cerr << "realloc: " << _max_image_size << " ->" << current_size << endl;
      _max_image_size = current_size;
      for (size_t i = 0; i<_num_threads; i++){
	_zbuffers_buf[i].reserve(_max_image_size);
	_indicess_buf[i].reserve(_max_image_size);
      }
    }
    for (size_t i = 0; i<_num_threads; i++){
      _zbuffers_buf[i].resize(current_size);
      _indicess_buf[i].resize(current_size);
      _zbuffers[i]=FloatImage(_image_rows, _image_cols, &(_zbuffers_buf[i][0]));
      _indicess[i]=IntImage(_image_rows, _image_cols, &(_indicess_buf[i][0]));
    }
  }


  PinholeProjector::PinholeProjector(){
    _K <<  
      525, 0, 319.5, 
      0, 525, 239.5, 
      0, 0, 1;
    _image_cols=640;
    _image_rows=480;
    _min_distance = 0.4;
    _max_distance = 3;
    setIncidenceAngle(0.5* M_PI);

    _max_image_size = 0;
#ifdef _GO_PARALLEL_
    _num_threads = omp_get_max_threads();
    _zbuffers.resize(_num_threads);
    _indicess.resize(_num_threads);
    _zbuffers_buf.resize(_num_threads);
    _indicess_buf.resize(_num_threads);
    cerr << "parallel projector initialized with " << _num_threads << " threads" << endl;
#else //_GO_PARALLEL_
    _num_threads = 1;
    _max_image_size = 0;
    cerr << "sequential projector initialized" << endl;
#endif //_GO_PARALLEL_
    _information_criterion=InverseDepth;
  }

#ifndef _GO_PARALLEL_
  void PinholeProjector::project(FloatImage& zbuffer, IndexImage& indices,
			    const Eigen::Isometry3f& T,
			    const Cloud3D& model) const {
    _project(zbuffer, indices, _inverse_offset*T, model, 0, model.size());
    return;
  }

#else //_GO_PARALLEL_
  void PinholeProjector::project(FloatImage& zbuffer, IndexImage& indices,
			    const Eigen::Isometry3f& T,
			    const Cloud3D& model) const {


    int iterationsPerThread =  model.size() / _num_threads;
    allocateBuffers();
#pragma omp parallel
    {
      int threadId = omp_get_thread_num();
      int imin = iterationsPerThread * threadId;
      int imax = imin + iterationsPerThread;
      if((size_t)imax > model.size())
	imax = model.size();
      _project(_zbuffers.at(threadId), _indicess[threadId], _inverse_offset*T, model, imin, imax, false);
    }
    
    // cerr << "reduce" << endl;
    zbuffer.create(_image_rows, _image_cols);
    indices.create(_image_rows, _image_cols);

# pragma omp parallel for
    for (int r=0; r<zbuffer.rows; r++) {
      float *thread_z_ptr[_num_threads];
      int   *thread_idx_ptr[_num_threads];
      // prepare the row indices
      float* z_ptr = zbuffer.ptr<float>(r);
      int* idx_ptr = indices.ptr<int>(r);
      for (int k=0; k<_num_threads; k++){
	thread_z_ptr[k] = _zbuffers[k].ptr<float>(r);
	thread_idx_ptr[k] = _indicess[k].ptr<int>(r);
      }

      for (int c=0; c<zbuffer.cols; c++) {
	float& depth = *z_ptr;
	int& idx = *idx_ptr;
	idx = -1;
	depth = 1e9f;
	for (int k=0; k<_num_threads; k++) {
	  float cdepth = *thread_z_ptr[k];
	  int cidx = *thread_idx_ptr[k];
	  if (cdepth<depth) {
	    depth = cdepth;
	    idx = cidx;
	  }
	  thread_z_ptr[k]++;
	  thread_idx_ptr[k]++;
	}
	z_ptr ++;
	idx_ptr ++;
      }
    }
  }

#endif // _GO_PARALLEL_
  void PinholeProjector::setCameraInfo(BaseCameraInfo* camera_info) {
    if (! camera_info) {
      _camera_info = 0;
      return;
    }
    PinholeCameraInfo* cam=dynamic_cast<PinholeCameraInfo*>(camera_info);
    if (! cam) {
      throw std::runtime_error("wrong type of camera for pinhole projector");
    }
    _raw_depth_scale=cam->depthScale();
    _camera_info = cam;
    _K=cam->cameraMatrix();
    setOffset(cam->offset());
  }

  bool PinholeProjector::supportsCameraInfo(BaseCameraInfo* camera_info) const {
    PinholeCameraInfo* cam=dynamic_cast<PinholeCameraInfo*>(camera_info);
    return cam;
  }
  
  void PinholeProjector::_project(FloatImage& zbuffer, IndexImage& indices,
				  const Eigen::Isometry3f& T,
				  const Cloud3D& model, int imin, int imax, 
				  bool allocate_images) const {
    if (allocate_images ) {
      zbuffer.create(_image_rows, _image_cols);
      indices.create(_image_rows, _image_cols);
    }
    
    zbuffer = 1e9f;
    indices = -1;
    const Eigen::Matrix3f Kr=_K*T.linear();
    const Eigen::Vector3f Kt=_K*T.translation();
    for (int idx = imin; idx<(size_t)imax; idx++){
      const Eigen::Vector3f p=Kr*model[idx].point()+Kt;
      const float& z=p.z();
      if (z<_min_distance || z>_max_distance) 
	continue;

      const float iz=1.f/z;
      int c = p.x()*iz+0.5; // rounding
      int r = p.y()*iz+0.5;
      if (r<0 || r>=_image_rows ||
	  c<0 || c>=_image_cols)
	continue;
      float& dest_z=zbuffer.ptr<float>(r)[c];
      {
	if (dest_z>z){
	  dest_z=z;
	  indices.ptr<int>(r)[c]=idx;
	}
      }
    }
  }

  void PinholeProjector::scaleCamera(float s) {
    _K.block<2,3>(0,0)*=s;
    _K(0,2)=.5*_image_cols;
    _K(1,2)=.5*_image_rows;
    _K.row(2) << 0,0,1;
  }


  PinholeProjector::State::State(const Eigen::Matrix3f& K, const Eigen::Isometry3f& offset, int r, int c) {
    this->K=K;
    this->offset = offset;
    rows = r;
    cols = c;
  }

  void PinholeProjector::pushState() {
    State s(_K, offset(), _image_rows, _image_cols);
    _states.push(s);
  }

  void PinholeProjector::popState() {
    State s = _states.top();
    _K = s.K;
    setOffset(s.offset);
    _image_rows = s.rows;
    _image_cols = s.cols;
    _states.pop();
  }


  void PinholeProjector::unprojectPoints(const RawDepthImage& depth_image) {
    // generate the points applying the inverse depth model
    Eigen::Matrix3f iK=_K.inverse();
    _points.create(depth_image.rows, depth_image.cols);
    _points=cv::Vec3f(0,0,0);
    for (int r=0; r<depth_image.rows; r++) {
      const unsigned short* id_ptr  = depth_image.ptr<unsigned short>(r);
      cv::Vec3f* dest_ptr = _points.ptr<cv::Vec3f>(r);
      for (int c=0; c<depth_image.cols; c++) {
	unsigned short id = *id_ptr;
	cv::Vec3f& dest = *dest_ptr;
	float d = id * _raw_depth_scale;
	Eigen::Vector3f p = iK * Eigen::Vector3f(c*d,r*d,d);
	if (id>0 && d<_max_distance && d>_min_distance) {
	  dest = cv::Vec3f(p.x(), p.y(), p.z());
	}
	else {
	  dest = cv::Vec3f(0,0,0);
	}
	id_ptr++;
	dest_ptr++;
      }
    }
  }

}
