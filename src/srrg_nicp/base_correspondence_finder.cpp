#include "base_correspondence_finder.h"
#include "base_solver.h"

namespace srrg_nicp {
  
  BaseCorrespondenceFinder::BaseCorrespondenceFinder(BaseSolver* s) {
    _solver = s;
    _normal_angle = .5;
    _points_distance = 0.2;
  }

  BaseCorrespondenceFinder::~BaseCorrespondenceFinder() {}

}
