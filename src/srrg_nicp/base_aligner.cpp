#include "base_aligner.h"

namespace srrg_nicp {

  using namespace std;
  using namespace srrg_boss;
  using namespace srrg_core;
  
  BaseAligner::Trigger::Trigger(BaseAligner* t, int e, int p) {
    _aligner = t;
    _event = e;
    _priority = p;
    cerr << "Trigger ctor, Events: " << e << endl;
    for (BaseAligner::EventTriggeMap::iterator it =_aligner->_triggers.begin();
	 it!=_aligner->_triggers.end(); it++){
      if ( (e & it->first) ) {
	BaseAligner::PriorityTriggerMap::iterator pit = it->second.find(_priority);
	if (pit != it->second.end())
	  throw std::runtime_error("error, trigger with the same priority already exists");
	it->second.insert(make_pair(_priority, this));
	cerr << "  adding aligner trigger:" << this << " for event: " << it->first << " priority: " << p;
      }
    }
  }

  BaseAligner::Trigger::~Trigger() {
    for (BaseAligner::EventTriggeMap::iterator it =_aligner->_triggers.begin();
	 it!=_aligner->_triggers.end(); it++){
      if (_event & it->first ) {
	BaseAligner::PriorityTriggerMap::iterator pit = it->second.find(_priority);
	if (pit == it->second.end())
	  throw std::runtime_error("error, deleting a non existing trigger");
	it->second.erase(pit); 
	cerr << "destroying trigger" << endl;
      }
    }
 }

  BaseAligner::BaseAligner(BaseSolver* solver, BaseProjector* proj):
    _solver(solver),
    _projector(proj){
    _solver = solver;
    _solver->setMaxError(.01);
    _solver->setDamping(100);
    _solver->setGICP(false);
    _reference_compression_enabled = false;
    _reference_canvas_scale = 1.5f;
    
    // populate trigger map
    _triggers.insert(make_pair(Initialized, PriorityTriggerMap()));
    _triggers.insert(make_pair(Correspondences, PriorityTriggerMap()));
    _triggers.insert(make_pair(Iteration, PriorityTriggerMap()));
    _triggers.insert(make_pair(Optimization, PriorityTriggerMap()));
    _reference_unchanged_hint=false;
  }

  BaseAligner::~BaseAligner(){}
  

  void BaseAligner::setCurrentModel( const Cloud3D* m) {
    _solver->setCurrentModel(m);
  }
  
  void BaseAligner::setReferenceModel( const Cloud3D* m) {
    _solver->setReferenceModel(m);
    _reference_unchanged_hint=false;
  }

  void BaseAligner::callTriggers(TriggerEvent event, void* params){
    BaseAligner::EventTriggeMap::iterator it = _triggers.find(event);
    if (it == _triggers.end()) {
      throw std::runtime_error("error, unsupported event in aligner");
    }
    for (PriorityTriggerMap::iterator pit = it->second.begin(); pit!= it->second.end(); pit++){
      pit->second->action(event, params);
    }
  }

}
