# srrg_nicp

**NICP** is a variant of the well know _ICP_ (Iterative Closest Point) 
algorithm. ICP is an iterative algorithm that refines an initial estimate of 
the relative transformation between two point clouds. At each step the 
algorithm tries to match pairs of points between the two clouds starting from 
a transform estimate. Minimizing the Euclidean distance between corresponding 
points leads to a better transformation that will be used as initial guess in 
the next iteration of the algorithm.

Differently from ICP, **NICP** considers each point together with the local 
features of the surface (normal and curvature) and it takes advantage of the 3D 
structure around the points for the determination of the data association 
between two point clouds. Moreover, it is based on a least squares formulation 
of the alignment problem, that minimizes an augmented error metric depending 
not only on the point coordinates but also on these surface characteristics.

## Prerequisites

requires:
* [srrg_boss](https://gitlab.com/srrg-software/srrg_boss)
* [srrg_core](https://gitlab.com/srrg-software/srrg_core)
* [srrg_core_map](https://gitlab.com/srrg-software/srrg_core_map)
* [srrg_cmake_modules](https://gitlab.com/srrg-software/srrg_cmake_modules)

## Examples 
`srrg_nicp` builds a bunch of examples to help using the libraries.
These include:
* `srrg_nicp_projective_aligner_example`: how to use the projective aligner
* `srrg_nicp_spherical_aligner_example`: how to use the spherical aligner
* `srrg_nicp_nn_aligner_example`: how to use the nn (nearest neighbor) aligner
* `srrg_nicp_3dscan2cloud_example`: how to convert the 3D scan to Cloud


## Authors
* Jacopo Serafin
* Giorgio Grisetti

## Related Publications

* Jacopo Serafin and Giorgio Grisetti. "[**Using extended measurements and scene merging for efficient and robust point cloud registration**](http://www.sciencedirect.com/science/article/pii/S0921889015302712)", Robotics and Autonomous Systems, 2017.
* Jacopo Serafin and Giorgio Grisetti. "[**NICP: Dense Normal Based Point Cloud Registration.**](http://ieeexplore.ieee.org/document/7353455/)" In Proc. of the International Conference on Intelligent Robots and Systems (IROS), Hamburg, Germany, 2015.
